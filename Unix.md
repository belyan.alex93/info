mkdir [dirname] - Создать папку

rmdir [dirname] - Удалить папку

touch [filename] - Создать файл

cat [filename] - Вывести в терминал содержимое файла

rm [filename] - Удалить файл

rm -rf [dirname] - Удалить папку(рекурсивно удаляет файлы)

cd [dirname] - Перейти в указанную директорию

ls (-a) - Вывести в терминал содержимое директории (-а выводит все файлы и папки включая скрытые и системные)

pwd - Вывести путь к текущей директории

mv [filename] [dirname]|[new_filename]> — Переместить (переименовать) файл

cp [filename] [dirname] — Копировать файл

gzip [filename] — Сжать файл

gunzip [filename].gz —  Разжать файл

tail -f [filename.log] - Открыть логфайл в реальном времени
